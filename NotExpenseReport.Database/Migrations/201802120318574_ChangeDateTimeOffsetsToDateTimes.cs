namespace NotExpenseReport.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDateTimeOffsetsToDateTimes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExpenseReports", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ExpenseReports", "ApprovalOrRejectionDate", c => c.DateTime());
            AlterColumn("dbo.ExpenseReports", "PaymentDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExpenseReports", "PaymentDate", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.ExpenseReports", "ApprovalOrRejectionDate", c => c.DateTimeOffset(precision: 7));
            AlterColumn("dbo.ExpenseReports", "CreatedDate", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
    }
}
