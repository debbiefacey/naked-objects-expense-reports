namespace NotExpenseReport.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterEnumerationColumns : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExpenseReports", "State", c => c.String(maxLength: 8));
            AlterColumn("dbo.ExpenseTypes", "PriceType", c => c.String(maxLength: 16));
            AlterColumn("dbo.PaymentTypes", "ExtraChargeType", c => c.String(maxLength: 16));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PaymentTypes", "ExtraChargeType", c => c.Int(nullable: false));
            AlterColumn("dbo.ExpenseTypes", "PriceType", c => c.Int(nullable: false));
            AlterColumn("dbo.ExpenseReports", "State", c => c.Int(nullable: false));
        }
    }
}
