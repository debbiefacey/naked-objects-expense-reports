namespace NotExpenseReport.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 32),
                        LastName = c.String(nullable: false, maxLength: 32),
                        EmailAddress = c.String(nullable: false, maxLength: 128),
                        Address = c.String(maxLength: 512),
                        IsActive = c.Boolean(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExpenseReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        State = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ApprovalOrRejectionDate = c.DateTimeOffset(precision: 7),
                        ReasonForRejection = c.String(maxLength: 256),
                        PaymentDate = c.DateTimeOffset(precision: 7),
                        Comment = c.String(maxLength: 512),
                        AdvancedPayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentTypeCode = c.String(maxLength: 8),
                        EmployeeId = c.Int(nullable: false),
                        ProjectCode = c.String(nullable: false, maxLength: 8),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeCode)
                .ForeignKey("dbo.Projects", t => t.ProjectCode, cascadeDelete: true)
                .Index(t => t.PaymentTypeCode)
                .Index(t => t.EmployeeId)
                .Index(t => t.ProjectCode);
            
            CreateTable(
                "dbo.ExpenseLines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LineNumber = c.Int(nullable: false),
                        ExpenseDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Description = c.String(nullable: false, maxLength: 128),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comment = c.String(maxLength: 512),
                        ExpenseTypeCode = c.String(nullable: false, maxLength: 8),
                        ExpenseReportId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExpenseReports", t => t.ExpenseReportId, cascadeDelete: true)
                .ForeignKey("dbo.ExpenseTypes", t => t.ExpenseTypeCode)
                .Index(t => new { t.ExpenseReportId, t.LineNumber }, unique: true)
                .Index(t => t.ExpenseTypeCode);
            
            CreateTable(
                "dbo.ExpenseTypes",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 8),
                        Description = c.String(nullable: false, maxLength: 128),
                        PriceType = c.Int(nullable: false),
                        Price = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.PaymentTypes",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 8),
                        Description = c.String(nullable: false, maxLength: 128),
                        ExtraCharge = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExtraChargeType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Code);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 8),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Code)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.EmployeeProjects",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false),
                        ProjectCode = c.String(nullable: false, maxLength: 8),
                    })
                .PrimaryKey(t => new { t.EmployeeId, t.ProjectCode })
                .ForeignKey("dbo.Users", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.ProjectCode, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.ProjectCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeProjects", "ProjectCode", "dbo.Projects");
            DropForeignKey("dbo.EmployeeProjects", "EmployeeId", "dbo.Users");
            DropForeignKey("dbo.ExpenseReports", "ProjectCode", "dbo.Projects");
            DropForeignKey("dbo.ExpenseReports", "PaymentTypeCode", "dbo.PaymentTypes");
            DropForeignKey("dbo.ExpenseLines", "ExpenseTypeCode", "dbo.ExpenseTypes");
            DropForeignKey("dbo.ExpenseLines", "ExpenseReportId", "dbo.ExpenseReports");
            DropForeignKey("dbo.ExpenseReports", "EmployeeId", "dbo.Users");
            DropIndex("dbo.EmployeeProjects", new[] { "ProjectCode" });
            DropIndex("dbo.EmployeeProjects", new[] { "EmployeeId" });
            DropIndex("dbo.Projects", new[] { "Name" });
            DropIndex("dbo.ExpenseLines", new[] { "ExpenseTypeCode" });
            DropIndex("dbo.ExpenseLines", new[] { "ExpenseReportId", "LineNumber" });
            DropIndex("dbo.ExpenseReports", new[] { "ProjectCode" });
            DropIndex("dbo.ExpenseReports", new[] { "EmployeeId" });
            DropIndex("dbo.ExpenseReports", new[] { "PaymentTypeCode" });
            DropTable("dbo.EmployeeProjects");
            DropTable("dbo.Projects");
            DropTable("dbo.PaymentTypes");
            DropTable("dbo.ExpenseTypes");
            DropTable("dbo.ExpenseLines");
            DropTable("dbo.ExpenseReports");
            DropTable("dbo.Users");
        }
    }
}
