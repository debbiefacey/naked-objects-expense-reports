using NofExpenseReport.Model;
using NotExpenseReport.Database.Mappings;
using System.Data.Entity;

namespace NotExpenseReport.Database
{
    public class NofExpenseReportDbContext : DbContext
    {
        public NofExpenseReportDbContext(string nameOrConnectionstring) : base(nameOrConnectionstring) { }

        public NofExpenseReportDbContext() { }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<ExpenseReport> ExpenseReports { get; set; }

        public DbSet<ExpenseType> ExpenseTypes { get; set; }

        public DbSet<PaymentType> PaymentTypes { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Initialisation
            // Use the Naked Objects > DbInitialiser template to add an initialiser, then reference thus:

            // Database.SetInitializer(new MyDbInitialiser()); 
           
            modelBuilder.Configurations.Add(new EmployeeMapping());
            modelBuilder.Configurations.Add(new ExpenseLineMapping());
            modelBuilder.Configurations.Add(new ExpenseReportMapping());
            // modelBuilder.Configurations.Add(new ExpenseReportStateMapping());
            modelBuilder.Configurations.Add(new ExpenseTypeMapping());
            // modelBuilder.Configurations.Add(new ExtraChargeTypeMapping());
            modelBuilder.Configurations.Add(new PaymentTypeMapping());
            // modelBuilder.Configurations.Add(new PriceTypeMapping());
            modelBuilder.Configurations.Add(new ProjectMapping());
            modelBuilder.Configurations.Add(new UserMapping());
        }
    }
}