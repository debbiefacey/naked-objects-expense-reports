using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class ExpenseLineMapping : EntityTypeConfiguration<ExpenseLine>
    {
        public ExpenseLineMapping()
        {
            Property(x => x.Description).HasMaxLength(128).IsRequired();
            Property(x => x.Comment).HasMaxLength(512);
            Property(x => x.ExpenseTypeCode).HasMaxLength(8);
            HasRequired(x => x.ExpenseType).WithMany().WillCascadeOnDelete(false);
            HasRequired(x => x.ExpenseReport).WithMany(x => x.ExpenseLines);
            HasIndex(x => new { x.ExpenseReportId, x.LineNumber }).IsUnique();
        }
    }
}