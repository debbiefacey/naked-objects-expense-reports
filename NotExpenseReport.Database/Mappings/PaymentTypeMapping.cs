using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class PaymentTypeMapping : EntityTypeConfiguration<PaymentType>
    {
        public PaymentTypeMapping()
        {
            HasKey(x => x.Code);
            Property(x => x.Code).HasMaxLength(8).IsRequired();
            Property(x => x.Description).HasMaxLength(128).IsRequired();
            Property(x => x.ExtraChargeType).HasColumnName("ExtraChargeType").HasMaxLength(16);
        }
    }
}