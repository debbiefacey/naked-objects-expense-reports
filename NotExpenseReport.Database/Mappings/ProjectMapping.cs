using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class ProjectMapping : EntityTypeConfiguration<Project>
    {
        public ProjectMapping()
        {
            HasKey(x => x.Code);
            Property(x => x.Code).HasMaxLength(8).IsRequired();
            Property(x => x.Name).HasMaxLength(64).IsRequired();
            HasIndex(x => x.Name).IsUnique();
        }
    }
}