using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class ExpenseTypeMapping : EntityTypeConfiguration<ExpenseType>
    {
        public ExpenseTypeMapping()
        {
            HasKey(x => x.Code);
            Property(x => x.Code).HasMaxLength(8).IsRequired();
            Property(x => x.Description).HasMaxLength(128).IsRequired();
            Property(x => x.PriceType).HasColumnName("PriceType").HasMaxLength(16);
        }
    }
}