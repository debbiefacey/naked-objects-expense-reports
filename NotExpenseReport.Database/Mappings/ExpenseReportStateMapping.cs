using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class ExpenseReportStateMapping : ComplexTypeConfiguration<ExpenseReportState>
    {
        public ExpenseReportStateMapping()
        {
            Ignore(x => x.DisplayName);
        }
    }
}