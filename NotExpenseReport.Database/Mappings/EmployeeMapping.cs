using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace NotExpenseReport.Database.Mappings
{
    public class EmployeeMapping : EntityTypeConfiguration<Employee>
    {
        public EmployeeMapping()
        {            
            Property(x => x.Address).HasMaxLength(512).IsRequired();
            HasMany(x => x.Projects)
                .WithMany(x => x.Employees)
                .Map(config =>
                {
                    config.MapLeftKey("EmployeeId");
                    config.MapRightKey("ProjectCode");
                });
        }
    }
}