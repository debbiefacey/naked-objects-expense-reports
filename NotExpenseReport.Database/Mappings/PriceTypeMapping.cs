using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class PriceTypeMapping : ComplexTypeConfiguration<PriceType>
    {
        public PriceTypeMapping()
        {
            Ignore(x => x.DisplayName);
        }
    }
}