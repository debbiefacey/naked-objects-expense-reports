using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class ExtraChargeTypeMapping : ComplexTypeConfiguration<ExtraChargeType>
    {
        public ExtraChargeTypeMapping()
        {
            Ignore(x => x.DisplayName);
        }
    }
}