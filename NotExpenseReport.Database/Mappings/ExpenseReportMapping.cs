using System.Data.Entity.ModelConfiguration;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class ExpenseReportMapping : EntityTypeConfiguration<ExpenseReport>
    {
        public ExpenseReportMapping()
        {
            Property(x => x.Comment).HasMaxLength(512);
            Property(x => x.ReasonForRejection).HasMaxLength(256);
            Property(x => x.PaymentTypeCode).HasMaxLength(8);
            Property(x => x.ProjectCode).HasMaxLength(8);
            HasOptional(x => x.PaymentType).WithMany();
            HasRequired(x => x.Project).WithMany(x => x.ExpenseReports);
            HasRequired(x => x.Employee).WithMany(x => x.ExpenseReports);
            Property(x => x.State).HasColumnName("State").HasMaxLength(8);
        }
    }
}