using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure;
using NofExpenseReport.Model;

namespace NotExpenseReport.Database.Mappings
{
    public class UserMapping : EntityTypeConfiguration<User>
    {
        public UserMapping()
        {
            Property(x => x.FirstName).HasMaxLength(32).IsRequired();
            Property(x => x.LastName).HasMaxLength(32).IsRequired();
            Property(x => x.EmailAddress).HasMaxLength(128).IsRequired();
        }
    }
}