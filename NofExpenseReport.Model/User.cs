﻿using NakedObjects;
using System;
using System.ComponentModel.DataAnnotations;

namespace NofExpenseReport.Model
{
    public abstract class User
    {
        [NakedObjectsIgnore]
        public virtual int Id { get; set; }
        
        [MemberOrder(1.1)]
        public virtual string FirstName { get; set; }

        [MemberOrder(1.2)]
        public virtual string LastName { get; set; }

        [Hidden(WhenTo.Always)]
        public virtual string FullName => FirstName + " " + LastName;

        [MemberOrder(1.3)]
        [EmailAddress]
        public virtual string EmailAddress { get; set; }

        #region Complementary Methods

        public string Title()
        {
            return $"{FullName} ({Id})";
        } 

        #endregion
    }
}
