﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NofExpenseReport.Model
{
    // [ComplexType]
    public class ExpenseReportState: Enumeration<ExpenseReportState>
    {
        public static readonly ExpenseReportState
            Open = new ExpenseReportState("Open"),
            Issued = new ExpenseReportState("Issued"),
            Approved = new ExpenseReportState("Approved"),
            Rejected = new ExpenseReportState("Rejected"),
            Closed = new ExpenseReportState("Closed");

        private ExpenseReportState(string value) : base(value)
        {
        }
    }
}
