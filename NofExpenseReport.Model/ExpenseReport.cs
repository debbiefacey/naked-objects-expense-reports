﻿using NakedObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace NofExpenseReport.Model
{
    public class ExpenseReport
    {
        #region Injected Services

        public IExpenseReportRepository ExpenseReportRepository { set; protected get; }

        #endregion

        [NakedObjectsIgnore]
        public virtual int Id { get; set; }
               
        [MemberOrder(1)]
        [Disabled(WhenTo.OncePersisted)]
        [Hidden(WhenTo.UntilPersisted)]
        public virtual DateTime CreatedDate { get; set; }

        [MemberOrder(2)]
        [Disabled(WhenTo.OncePersisted)]
        [Hidden(WhenTo.UntilPersisted)]
        [DisplayName("Status")]
        public virtual string State { get; set; }

        [MemberOrder(3)]
        [Optionally]
        public virtual DateTime? ApprovalOrRejectionDate { get; set; }

        [MemberOrder(4)]
        [Optionally]
        public virtual string ReasonForRejection { get; set; }


        [MemberOrder(5)]
        [Optionally]
        public virtual DateTime? PaymentDate { get; set; }

        [MultiLine(NumberOfLines = 5)]
        [Optionally]
        public virtual string Comment { get; set; }

        /// <summary>
        /// The advance money given to an employee to cover anticipated expenses
        /// </summary>
        /// <value>
        /// The advanced payment.
        /// </value>
        [MemberOrder(1.1)]
        [Mask("C")]
        [Optionally]
        public virtual decimal AdvancedPayment { get; set; } = 0;

        /// <summary>
        /// Gets the balance owed to the employee.
        /// </summary>
        /// <value>
        /// The balance owed to the employee.
        /// </value>
        [MemberOrder(1.3)]
        [Mask("C")]
        [Hidden(WhenTo.UntilPersisted)]
        public virtual decimal Balance => TotalExpenses - AdvancedPayment;

        [Mask("C")]
        [MemberOrder(1.2)]
        public virtual decimal TotalExpenses => ExpenseLines.Sum(x => x.TotalExpense);
        
        [Eagerly(EagerlyAttribute.Do.Rendering)]
        [TableView(
            false, 
            nameof(ExpenseLine.LineNumber), 
            nameof(ExpenseLine.Description), 
            nameof(ExpenseLine.ExpenseType), 
            nameof(ExpenseLine.TotalExpense))
        ]
        public virtual ICollection<ExpenseLine> ExpenseLines { get; set; } = new List<ExpenseLine>();

        [NakedObjectsIgnore]
        public virtual string PaymentTypeCode { get; set; }

        [MemberOrder(0.3)]
        [Optionally]
        public virtual PaymentType PaymentType { get; set; }

        [NakedObjectsIgnore]
        public virtual int EmployeeId { get; set; }

        [MemberOrder(0.1)]
        public virtual Employee Employee { get; set; }

        [NakedObjectsIgnore]
        public virtual string ProjectCode { get; set; }

        [MemberOrder(0.2)]
        public virtual Project Project { get; set; }

        #region Actions
        
        [MemberOrder(1)]
        public ExpenseLine AddExpenseLine()
        {
            return ExpenseReportRepository.AddExpenseLine(this);
        }

        [MemberOrder(2)]
        public void Issue()
        {
            State = ExpenseReportState.Issued;
        }

        public void Approve()
        {
            State = ExpenseReportState.Approved;
            ApprovalOrRejectionDate = DateTime.UtcNow;
        }

        public void Reject(string rejectionReason)
        {
            State = ExpenseReportState.Rejected;
            ApprovalOrRejectionDate = DateTime.UtcNow;
            ReasonForRejection = rejectionReason;
        }

        #endregion

        #region Complementary Methods

        public string Title()
         => $"Expense Report: Project({Project?.Code}) Employee({Employee?.FullName})";

        public bool HideApprovalOrRejectionDate() =>
            Id == 0 || State == ExpenseReportState.Open || State == ExpenseReportState.Issued;

        public bool HideReasonForRejection() =>
            State != ExpenseReportState.Rejected;

        public bool HidePaymentDate() =>
            State != ExpenseReportState.Closed;

        public bool HideTotalExpenses() =>
            ExpenseLines.Count == 0;

        public string DisablePropertyDefault() => 
            Id != 0 && State != ExpenseReportState.Open ? 
            "Cannot edit an expense report that has been issued" : null;

        public string DisableIssue() =>
            !ExpenseLines.Any() ?
            "Expense Lines must be added before the expense report can be issued" : null;

        public bool HideAddExpenseLine() =>
             State != ExpenseReportState.Open;

        public bool HideIssue() =>
            State != ExpenseReportState.Open;

        public bool HideApprove() =>
            State != ExpenseReportState.Issued;

        public bool HideReject() =>
            State != ExpenseReportState.Issued;

        [PageSize(10)]
        public List<Project> ChoicesProject() =>
            // Get employee's projects
            Employee.Projects.ToList();

        #endregion

        #region Lifecycle

        public virtual void Persisting()
        {
            CreatedDate = DateTime.UtcNow;
            State = ExpenseReportState.Open;
        }


        #endregion
    }
}
