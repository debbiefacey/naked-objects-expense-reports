﻿using System.Linq;

namespace NofExpenseReport.Model
{
    public interface IPaymentTypeRepository
    {
        PaymentType NewPaymentType();

        IQueryable<PaymentType> AllPaymentTypes();
    }
}