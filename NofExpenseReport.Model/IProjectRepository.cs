﻿using System.Linq;
using NofExpenseReport.Model;

namespace NofExpenseReport.Model
{
    public interface IProjectRepository
    {
        IQueryable<Project> AllProjects();

        IQueryable<Project> FindProjects(string code = null, string name = null);

        Project NewProject();
    }
}