﻿using System;
using System.Linq;

namespace NofExpenseReport.Model
{
    public interface IExpenseReportRepository
    {
        ExpenseReport NewExpenseReport(Employee employee);

        ExpenseLine AddExpenseLine(ExpenseReport expenseReport);

        IQueryable<ExpenseReport> FindExpenseReports(
            string employeeName,
            string projectName,
            DateTime? fromCreateDate,
            DateTime? toCreateDate);
    }
}