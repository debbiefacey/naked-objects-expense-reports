﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NofExpenseReport.Model
{
    [ComplexType]
    public class PriceType : Enumeration<PriceType>
    {
        public static readonly PriceType
            None = new PriceType("None"),
            Fixed = new PriceType("Fixed"),
            Recommended = new PriceType("Recommended");

        public PriceType() { }

        private PriceType(string value) : base(value)
        {
        }
    }
}
