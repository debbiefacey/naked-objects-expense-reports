﻿using NakedObjects;

namespace NofExpenseReport.Model
{
    [Bounded]
    public class ExpenseType
    {
        [Disabled(WhenTo.OncePersisted)]
        [MemberOrder(1)]
        public virtual string Code { get; set; }

        [MemberOrder(2)]
        [Title]
        public virtual string Description { get; set; }

        [MemberOrder(4)]
        public virtual string PriceType { get; set; } = Model.PriceType.None;

        public string[] ChoicesPriceType() =>
            Model.PriceType.GetAllValues();

        [Mask("C")]
        [Optionally]
        [MemberOrder(3)]
        public virtual decimal? Price { get; set; }
    }
}
