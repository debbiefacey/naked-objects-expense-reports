﻿using NakedObjects;
using System.Linq;

namespace NofExpenseReport.Model
{
    [Bounded]
    public class PaymentType
    {
        [Disabled(WhenTo.OncePersisted)]
        [MemberOrder(1)]
        public virtual string Code { get; set; }

        [MemberOrder(2)]
        public virtual string Description { get; set; }

        [Mask("C")]
        [MemberOrder(3)]
        public virtual decimal ExtraCharge { get; set; } = 0;

        [MemberOrder(4)]
        public virtual string ExtraChargeType { get; set; } = Model.ExtraChargeType.Fixed;

        public string[] ChoicesExtraChargeType() =>
            Model.ExtraChargeType.GetAllValues();

        public override string ToString()
        {
            return $"{Description} ({Code})";
        }
    }
}
