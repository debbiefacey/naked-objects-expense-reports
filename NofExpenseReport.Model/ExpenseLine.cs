﻿using NakedObjects;
using System;

namespace NofExpenseReport.Model
{
    public class ExpenseLine
    {
        [NakedObjectsIgnore]
        public virtual int Id { get; set; }

        [MemberOrder(1)]
        public virtual int LineNumber { get; set; }
        
        [MemberOrder(2)]
        [Title]
        public virtual string Description { get; set; }

        [MemberOrder(3)]
        public virtual DateTimeOffset ExpenseDate { get; set; }

        [NakedObjectsIgnore]
        public virtual string ExpenseTypeCode { get; set; }

        [MemberOrder(4)]
        public virtual ExpenseType ExpenseType { get; set; }

        [MemberOrder(5)]
        public virtual int Quantity { get; set; } = 1;

        [Mask("C")]
        [MemberOrder(6)]
        public virtual decimal Price { get; set; }

        public string DisablePrice() => 
            ExpenseType?.PriceType == PriceType.Fixed ?
            "Cannot modify fixed-price expense type" : null;

        [Mask("C")]
        [MemberOrder(7)]
        public decimal TotalExpense => Quantity * Price;

        [MemberOrder(8)]
        [MultiLine(NumberOfLines = 5)]
        [Optionally]
        public virtual string Comment { get; set; }
        
        [NakedObjectsIgnore]
        public virtual int ExpenseReportId { get; set; }

        public virtual ExpenseReport ExpenseReport { get; set; }
    }
}
