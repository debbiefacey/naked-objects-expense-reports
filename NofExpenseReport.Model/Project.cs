﻿using NakedObjects;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NofExpenseReport.Model
{
    public class Project
    {
        #region Injected Services

        public IEmployeeRepository EmployeeRepository { set; protected get; }

        #endregion

        [Disabled(WhenTo.OncePersisted)]
        [MemberOrder(1)]        
        public virtual string Code { get; set; }

        [MemberOrder(2)]
        public virtual string Name { get; set; }

        [MemberOrder(3)]
        public virtual ICollection<ExpenseReport> ExpenseReports { get; set; } =
            new List<ExpenseReport>();

        [MemberOrder(4)]
        public virtual ICollection<Employee> Employees { get; set; } =
            new List<Employee>();

        #region Actions

        [MemberOrder(1)]
        public void AddEmployee(Employee employee)
        {
            if (!this.Employees.Any(x => x.Id == employee.Id))
            {
                this.Employees.Add(employee);
            }
        }

        #endregion

        #region Complementary Methods

        public string Title() => $"Project: {Name} ({Code})";
        
        public IQueryable<Employee> AutoComplete0AddEmployee([MinLength(3)]string name) =>
            EmployeeRepository.FindEmployees(name: name).Where(x => !x.Projects.Any(p => p.Code == this.Code));

        #endregion
    }
}
