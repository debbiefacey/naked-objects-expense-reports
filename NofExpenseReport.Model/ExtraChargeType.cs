﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NofExpenseReport.Model
{
    // [ComplexType]
    public class ExtraChargeType : Enumeration<ExtraChargeType>
    {
        public static readonly ExtraChargeType
            None = new ExtraChargeType("None"),
            Percentage = new ExtraChargeType("Percentage"),
            Fixed = new ExtraChargeType("Fixed");

        public ExtraChargeType(string value) : base(value)
        {
        }
    }
}
