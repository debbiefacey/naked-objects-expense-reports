﻿using System.Linq;

namespace NofExpenseReport.Model
{
    public interface IEmployeeRepository
    {
        Employee NewEmployee();
        
        IQueryable<Employee> FindEmployees(int? id = null, string name = null, string emailAddress = null);
    }
}