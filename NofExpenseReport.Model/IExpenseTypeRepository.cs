﻿using System.Linq;

namespace NofExpenseReport.Model
{
    public interface IExpenseTypeRepository
    {
        IQueryable<ExpenseType> AllExpenseTypes();

        ExpenseType NewExpenseType();
    }
}