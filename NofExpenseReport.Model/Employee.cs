﻿using NakedObjects;
using System.Collections.Generic;
using System.Linq;

namespace NofExpenseReport.Model
{
    public class Employee : User
    {
        #region Injected Services
        
        public IProjectRepository ProjectRepository { set; protected get; }
        
        #endregion

        [MultiLine(NumberOfLines = 4)]
        [MemberOrder(2.1)]        
        public virtual string Address { get; set; }
               
        [Mask("C")]
        [MemberOrder(2.2)]
        [Hidden(WhenTo.UntilPersisted)]
        public virtual decimal Balance => ExpenseReports.Sum(x => x.Balance);

        [MemberOrder(2.3)]
        public virtual bool IsActive { get; set; }

        [MemberOrder(2.41)]
        [Eagerly(EagerlyAttribute.Do.Rendering)]        
        public virtual ICollection<Project> Projects { get; set; } = new List<Project>();

        [MemberOrder(2.42)]
        [Eagerly(EagerlyAttribute.Do.Rendering)]
        [TableView(
            false, 
            nameof(ExpenseReport.Project), 
            nameof(ExpenseReport.CreatedDate), 
            nameof(ExpenseReport.State), 
            nameof(ExpenseReport.TotalExpenses), 
            nameof(ExpenseReport.Balance))
        ]
        public virtual ICollection<ExpenseReport> ExpenseReports { get; set; } = new List<ExpenseReport>();

        #region Actions

        [MemberOrder(1)]
        public void AssignToProject(Project project)
        {
            if (!this.Projects.Any(x => x.Code == project.Code))
            {
                this.Projects.Add(project);
            }
        }

        #endregion

        #region Complementary Methods

        public IQueryable<Project> Choices0AssignToProject() =>
            ProjectRepository.AllProjects().Where(x => !x.Employees.Any(e => e.Id == this.Id));

        #endregion
    }
}
