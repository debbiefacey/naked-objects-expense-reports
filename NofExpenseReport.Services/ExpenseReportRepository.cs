﻿using NakedObjects;
using NakedObjects.Services;
using NofExpenseReport.Model;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NofExpenseReport.Services
{
    public class ExpenseReportRepository : AbstractFactoryAndRepository, IExpenseReportRepository
    {
        #region Injected Services

        public IEmployeeRepository EmployeeRepository { set; protected get; }

        #endregion

        [MemberOrder(1)]           
        public ExpenseReport NewExpenseReport(Employee employee)
        {
            var expenseReport = NewTransientInstance<ExpenseReport>();            
            expenseReport.Employee = employee;
           
            return expenseReport;
        }
        [MemberOrder(2)]
        [DisplayName("Search")]
        [TableView(
            false, 
            nameof(ExpenseReport.Employee), 
            nameof(ExpenseReport.Project), 
            nameof(ExpenseReport.CreatedDate), 
            nameof(ExpenseReport.State), 
            nameof(ExpenseReport.TotalExpenses), 
            nameof(ExpenseReport.Balance))
        ]
        public IQueryable<ExpenseReport> FindExpenseReports(
            [Optionally]string employeeName,
            [Optionally]string projectName,
            [Optionally]DateTime? fromCreateDate,
            [Optionally]DateTime? toCreateDate)
        {
            return Container.Instances<ExpenseReport>()
                .Where(x => employeeName == null 
                    || (x.Employee.FirstName + " " + x.Employee.LastName).StartsWith(employeeName)
                    || x.Employee.LastName.StartsWith(employeeName)
                 )               
                .Where(x => projectName == null || x.Project.Name.StartsWith(projectName))
                .Where(x => fromCreateDate == null || x.CreatedDate >= fromCreateDate)
                .Where(x => toCreateDate == null || x.CreatedDate <= toCreateDate);
        }
        
        [Hidden(WhenTo.Always)]
        public ExpenseLine AddExpenseLine(ExpenseReport expenseReport)
        {
            // get next line number
            var lastLineNumber =
                Container.Instances<ExpenseLine>()
                    .Where(x => x.ExpenseReportId == expenseReport.Id)
                    .Max(x => (int?)x.LineNumber) ?? 0;

            var expenseLine = NewTransientInstance<ExpenseLine>();
            expenseLine.ExpenseReport = expenseReport;
            expenseLine.LineNumber = lastLineNumber + 1;

            return expenseLine;
        }
        
        #region Complementary Methods

        [PageSize(10)]
        public IQueryable<Employee> AutoComplete0NewExpenseReport([MinLength(2)] string name) =>
            EmployeeRepository.FindEmployees(name: name);

        #endregion
    }
}
