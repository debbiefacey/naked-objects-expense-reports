﻿using NakedObjects;
using NakedObjects.Services;
using NofExpenseReport.Model;
using System.ComponentModel;
using System.Linq;

namespace NofExpenseReport.Services
{
    public class ProjectRepository : AbstractFactoryAndRepository, IProjectRepository
    {    
        [MemberOrder(1)]
        public Project NewProject()
        {
            return NewTransientInstance<Project>();
        }

        [MemberOrder(2)]
        [DisplayName("List All")]
        public IQueryable<Project> AllProjects()
        {
            return Container.Instances<Project>();
        }

        [MemberOrder(3)]
        [DisplayName("Search")]        
        public IQueryable<Project> FindProjects([Optionally]string code = null, [Optionally]string name = null)
        {
            return Container.Instances<Project>()
                .Where(x => code == null || x.Code.StartsWith(code))
                .Where(x => name == null || x.Name.StartsWith(name));
        }
    }
}
