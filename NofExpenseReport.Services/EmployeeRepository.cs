﻿using NakedObjects;
using NakedObjects.Services;
using NofExpenseReport.Model;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NofExpenseReport.Services
{
    public class EmployeeRepository : AbstractFactoryAndRepository, IEmployeeRepository
    {
        #region Injected Services
                
        #endregion

        [MemberOrder(1)]
        public Employee NewEmployee()
        {
            return NewTransientInstance<Employee>();
        }
        
        [MemberOrder(2)]
        [DisplayName("Search")]
        public IQueryable<Employee> FindEmployees(
            [Optionally] int? id = null,
            [Optionally] string name = null,
            [Optionally] string emailAddress = null
        ) =>
            Container.Instances<Employee>()
            .Where(x => id == null || x.Id == id.Value)
            .Where(x => name == null 
                || (x.FirstName + " " + x.LastName).StartsWith(name) 
                || x.LastName.StartsWith(name)
            )       
            .Where(x => emailAddress == null || x.EmailAddress == emailAddress);
                
        #region Complementary Methods

        #endregion
    }
}
