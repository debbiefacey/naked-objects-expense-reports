﻿using NakedObjects;
using NakedObjects.Services;
using NofExpenseReport.Model;
using System.ComponentModel;
using System.Linq;

namespace NofExpenseReport.Services
{
    public class ExpenseTypeRepository : AbstractFactoryAndRepository, IExpenseTypeRepository
    {    
        [MemberOrder(1)]
        public ExpenseType NewExpenseType()
        {
            return NewTransientInstance<ExpenseType>();
        }

        [MemberOrder(2)]
        [DisplayName("List All")]
        [TableView(
            false, 
            nameof(ExpenseType.Code), 
            nameof(ExpenseType.Description), 
            nameof(ExpenseType.Price), 
            nameof(ExpenseType.PriceType))
        ]
        public IQueryable<ExpenseType> AllExpenseTypes()
        {
            return Container.Instances<ExpenseType>();
        }
    }
}
