﻿using NakedObjects;
using NakedObjects.Services;
using NofExpenseReport.Model;
using System.ComponentModel;
using System.Linq;

namespace NofExpenseReport.Services
{
    public class PaymentTypeRepository : AbstractFactoryAndRepository, IPaymentTypeRepository
    {
        [MemberOrder(1)]
        public PaymentType NewPaymentType()
        {
            return NewTransientInstance<PaymentType>();
        }

        [MemberOrder(2)]
        [DisplayName("List All")]
        [TableView(
            false,
            nameof(PaymentType.Code), 
            nameof(PaymentType.Description),
            nameof(PaymentType.ExtraCharge), 
            nameof(PaymentType.ExtraChargeType))]
        public IQueryable<PaymentType> AllPaymentTypes()
        {
            return Container.Instances<PaymentType>();
        }
    }
}
